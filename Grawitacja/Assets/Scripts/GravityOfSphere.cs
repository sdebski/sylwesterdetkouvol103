﻿using UnityEngine;

public class GravityOfSphere : MonoBehaviour
{
    private const float GravitationalConstant = 5;
    private const float PullRadius = 100;
    private const float MinRadius = 0.001f;
    private float _gravitationalPull;
    private float _massOfSphere;

    public LayerMask Mask;
    private Rigidbody _rigidbody;

	void Start ()
	{
	    _rigidbody = GetComponent<Rigidbody>();
	    _massOfSphere = _rigidbody.mass;
	    
	}

    void FixedUpdate()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, PullRadius, Mask);
        foreach (var item in colliders)
        {
            Rigidbody otherRigidbody = item.GetComponent<Rigidbody>();
            if (otherRigidbody == null)
                continue;

            var direction = transform.position - item.transform.position;

            if (direction.magnitude < MinRadius)
                continue;
           
            
            _gravitationalPull =
                ((_massOfSphere * otherRigidbody.mass) / direction.sqrMagnitude) * GravitationalConstant;
            otherRigidbody.AddForce(direction*_gravitationalPull*Time.fixedDeltaTime);
        }
    }

   
}
