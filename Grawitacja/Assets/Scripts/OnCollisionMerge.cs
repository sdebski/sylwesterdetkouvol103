﻿using UnityEngine;

public class OnCollisionMerge : MonoBehaviour
{

    public bool IsAbsorbed;
    private Rigidbody _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }


    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<OnCollisionMerge>().IsAbsorbed) return;
        IsAbsorbed = true;
        other.rigidbody.mass += _rigidbody.mass;
        other.transform.localScale += _rigidbody.transform.localScale;
        CreateSphere.ActualListOfSpheres.Remove(gameObject);
        Destroy(gameObject);
        
    }
}
