﻿using UnityEngine;
using UnityEngine.UI;

public class SphereCounter : MonoBehaviour
{
    private Text _text;

    void Awake()
    {
        _text = GetComponent<Text>();
    }
	void Update ()
	{
	    _text.text = "Spheres on scene: " + CreateSphere.ActualListOfSpheres.Count;
	}
}
