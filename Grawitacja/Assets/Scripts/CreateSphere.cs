﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateSphere : MonoBehaviour
{
    public GameObject SpherePrefab;
    public Camera Camera;

    public static readonly List<GameObject> ListOfSpheres = new List<GameObject>();
    public static readonly List<GameObject> ActualListOfSpheres = new List<GameObject>();

    private IEnumerator _coroutine;
	
	void Start ()
	{
		_coroutine = InstantiateSphere(0.25f);
		StartCoroutine(_coroutine);
	}
	
	private IEnumerator InstantiateSphere(float waitTime)
	{
		while (ListOfSpheres.Count < 250)
		{
			yield return new WaitForSeconds(waitTime);
		    var newObject = Instantiate(SpherePrefab, RandInCamera(), Quaternion.identity);
			ListOfSpheres.Add(newObject);
		    ActualListOfSpheres.Add(newObject);

		}	
	}

	private Vector3 RandInCamera()
	{
		Vector3 cameraView = Camera.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width),
			Random.Range(0, Screen.height), Camera.farClipPlane/4));
		return cameraView;
	}
}
